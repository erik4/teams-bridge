from fastapi import FastAPI
from main.routes import init_routes
import logging
from logging.config import dictConfig
import config
from config import LogConfig

dictConfig(LogConfig().dict())
logger = logging.getLogger(config.settings.app_name)

logger.info(f"Starting {config.settings.app_name} version {config.settings.app_version}.")

app = FastAPI(
    title=config.settings.app_name,
    description=config.settings.app_description,
    version=config.settings.app_version,
)

init_routes(app)
