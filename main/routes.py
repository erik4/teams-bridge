# import uuid
# from typing import List
import logging
import config
from fastapi import Depends, HTTPException, Request, Body
from backends import TeamsBackend

# from . import schemas
logger = logging.getLogger(config.settings.app_name)


def init_routes(app):

    @app.post("/messages/inbound/", status_code=200, tags=["webhooks"])
    async def inbound_from_cm(payload: dict = Body(...)):
        logger.debug(payload)
        await TeamsBackend.convert_inbound(payload)

    @app.post("/messages/outbound/", tags=["webhooks"])
    async def outbound_from_teams(payload):
        logger.debug(payload)
        await TeamsBackend.convert_outbound(payload)
