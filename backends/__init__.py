import asyncio


class TeamsBackend:

    def __init__(self):
        pass

    @staticmethod
    async def convert_inbound(data):
        """
        Converts inbound messages from CM.COM to Microsoft Teams
        :param data: CM.COM MO data
        :return:
        """
        await asyncio.sleep(0)

    @staticmethod
    async def convert_outbound(data):
        """
        Converts outbound messages from Microsoft Teams to CM.COMs Business Messaging API
        :param data: Teams message
        :return:
        """
        await asyncio.sleep(0)
