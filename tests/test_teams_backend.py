import pytest
from backends import TeamsBackend


# this fixture configures what async backend anyio uses for async tests
@pytest.fixture
def anyio_backend():
    return 'asyncio'


@pytest.mark.anyio
async def test_root():
    response = await TeamsBackend.convert_inbound("{}")
    assert response is None


def func(x):
    return x + 1


def test_answer():
    assert func(3) == 4
