import os
from pydantic import BaseSettings, BaseModel
from dotenv import load_dotenv

load_dotenv()


class Settings(BaseSettings):
    app_name: str = "Teams Bridge"
    app_description: str = "Bridge between teams and CM.COM Business Messaging API"
    app_version: str = "1.0.0"
    cm_product_token: str = os.getenv('CM_PRODUCT_TOKEN')
    # database_url: str = os.getenv('DATABASE_URI') or "sqlite:///./sql_app.db"
    connection_args: dict = {"check_same_thread": False}


settings = Settings()


class LogConfig(BaseModel):
    """Logging configuration to be set for the server"""

    LOGGER_NAME: str = settings.app_name
    LOG_FORMAT: str = "%(levelprefix)s | %(asctime)s | %(message)s"
    LOG_LEVEL: str = "DEBUG"

    # Logging config
    version = 1
    disable_existing_loggers = False
    formatters = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    handlers = {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
    }
    loggers = {
        settings.app_name: {"handlers": ["default"], "level": LOG_LEVEL},
    }
