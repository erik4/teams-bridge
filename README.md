# Teams Bridge

More info on the Microsoft Graph API for teams chats:
- https://docs.microsoft.com/en-us/graph/api/resources/teams-api-overview?view=graph-rest-1.0
- https://docs.microsoft.com/en-us/graph/api/chatmessage-get?view=graph-rest-1.0&tabs=http

Specific chats:
- https://docs.microsoft.com/en-us/microsoftteams/platform/graph-api/rsc/resource-specific-consent

Resource specific settings for a chat:
- https://docs.microsoft.com/en-us/microsoftteams/platform/graph-api/rsc/resource-specific-consent#resource-specific-permissions-for-a-chat

Testing inbound using NGROK:
- ./ngrok http -subdomain=yourcustomsubdomain 8000
Send message to @AliceMi80086068

Example incoming data:

```json
{
   "reference":"9894",
   "messageContext":"",
   "from":{
      "number":"42621586",
      "name":"@precompiled"
   },
   "to":{
      "number":"1376895359293751308"
   },
   "message":{
      "text":"Hi Alice, what's up?",
      "media":{
         "mediaUri":"",
         "contentType":"",
         "title":""
      },
      "custom":{
         
      }
   },
   "groupings":[
      "",
      "",
      ""
   ],
   "time":"2022-02-04 19:59:21",
   "timeUtc":"2022-02-04T18:59:21",
   "channel":"Twitter"
}
```

Message with image

```json
{
   "reference":"15024",
   "messageContext":"",
   "from":{
      "number":"42621586",
      "name":"@precompiled"
   },
   "to":{
      "number":"1376895359293751308"
   },
   "message":{
      "text":"Let me send you a message with an image, ",
      "media":{
         "mediaUri":"http://api.cm.com/resources/v1.0/files/fc84b613-6b74-4fec-8d2d-ee221f9c5973/content",
         "contentType":"image/png",
         "title":"bvvq3iOj.png"
      },
      "custom":{
         
      }
   },
   "groupings":[
      "",
      "",
      ""
   ],
   "time":"2022-02-04 20:03:15",
   "timeUtc":"2022-02-04T19:03:15",
   "channel":"Twitter"
}
```

Resource specific consent
- https://docs.microsoft.com/nl-nl/microsoftteams/platform/graph-api/rsc/resource-specific-consent
App that needs to post a message in a channel:
- https://stackoverflow.com/questions/67542543/how-to-use-node-js-to-create-a-new-chat-message-in-an-existing-channel-in-teams
Authorization code flow:
- https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow
Access on behalf:
- https://docs.microsoft.com/en-us/graph/auth-v2-user?view=graph-rest-beta
